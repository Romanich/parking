﻿using System;
using System.Text;
using static Parking.Settings;

namespace Parking
{
    class ParkingSlot
    {
        public Vehicle SlotVehicle { get; private set; }
        public Balance SlotBalance { get; }
        public DateTime RegistrationTime { get; private set; }

        public ParkingSlot()
        {
            SlotBalance = new Balance(0m);
        }

        public void SetVehicle(Vehicle vehicle)
        {
            SlotVehicle = new Vehicle(vehicle.VehicleType);
            RegistrationTime = DateTime.Now;
            Console.WriteLine($"The {SlotVehicle.VehicleType} with number {SlotVehicle.Id} was parked.", SlotVehicle);
        }

        public string GetSlotInfoDetails()
        {
            StringBuilder result = new StringBuilder();
            result.AppendLine($"Vehicle type: {SlotVehicle.VehicleType}");
            result.AppendLine($"Vehicle ID: {SlotVehicle.Id}");
            result.AppendLine($"Parked date and time: {RegistrationTime}");
            result.AppendLine($"Incomes from this vehical: {SlotBalance.GetBalance()} {Settings.СurrencyName}");
            return result.ToString();
        }

        public string GetVehiclelId()
        {
            StringBuilder result = new StringBuilder();
            result.Append($"{SlotVehicle.VehicleType}, {SlotVehicle.Id}");
            return result.ToString();
        }

        public decimal CalculateCommonCharge()
        {
             decimal commonCharge = GetIntervalChargeCost(SlotVehicle.VehicleType);
             return commonCharge;
        }

        public decimal CalculateFineCharge()
        {
            decimal finedCharge = decimal.Multiply(GetIntervalChargeCost(SlotVehicle.VehicleType), Settings.OverusedCostTaxRate);
            return finedCharge;
        }

        public decimal GetSlotCharge()
        {
            decimal commonCharge = CalculateCommonCharge();
            decimal finedCharge = CalculateFineCharge();

            if (SlotVehicle.Balance.Withdraw(commonCharge))
            {
                SlotBalance.TopUp(commonCharge);
                return commonCharge;
            }
            else
            {
                SlotVehicle.Balance.WithdrawWithDebt(finedCharge);
                SlotBalance.TopUp(finedCharge);
                return finedCharge;
            }
        }
    }
}
