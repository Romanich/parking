﻿using System;

namespace Parking
{
    class Program
    {
        static void Main(string[] args)
        {
            var parking = Parking.GetInstance();

            TransactionLogger Logger = TransactionLogger.GetInstance();

            bool continueFlow = true;
            while (continueFlow)
            {
                continueFlow = MainMenu(Logger);
            }
            Console.Write("Press any key to exit... ");
            Console.ReadLine();
        }

        static bool MainMenu(TransactionLogger logger, bool ClearConsole = true)
        {
            var parking = Parking.GetInstance();

            Console.WriteLine();
            Console.WriteLine("MAIN MENU");
            Console.WriteLine("Make your choise (input number/letter) and press ENTER).");
            Console.WriteLine("1. Show parking balance");
            Console.WriteLine("2. Show last minute parking incomes");
            Console.WriteLine("3. Show current free/used parking places");
            Console.WriteLine("4. Show all parking slots info");
            Console.WriteLine("5. Add vehicle");
            Console.WriteLine("6. Remove vehicle");
            Console.WriteLine("7. Top up balance of parking slot");
            Console.WriteLine("8. Show parking's last minute transactions");
            Console.WriteLine("9. Show transaction info from logfile");
            Console.WriteLine("Q. Quit");
            Console.WriteLine();
            var variant = Console.ReadLine();
            if (ClearConsole) Console.Clear();

            switch (variant.ToLower())
            {
                case "1": parking.ShowParkingBalance(); break;
//                case "2": parking.ShowIncomeSum((DateTime.Now.AddMinutes(-1))); break;
                case "3": parking.ShowFreeAndUsedSlots(); break;
                case "4": parking.ShowAllSlotsDetails(); break;
                case "5": parking.ParkVehicle(); break;
                case "6": parking.RemoveVehicle(); break;
                case "7": parking.TopUpVehicleBalance(); break;
                case "8": logger.ReadTransactionsForPeriod(DateTime.Now.AddMinutes(-1)); break;
                case "9": logger.ReadLog(); break;
                case "q": return false;
                default: Console.WriteLine("Check your input and try again."); break;
            }
            return true;
        }
    }
}
