﻿using System;

namespace Parking
{
    public class Balance
    {
        public delegate void BalanceHandler(object obj, BalanceEventArg e);
        public event BalanceHandler ToppedUp;
        public event BalanceHandler Withdrawn;

        private decimal ownFunds;
        private decimal debtFunds;

        public Balance(decimal balance)
        {
            this.ownFunds = balance;
            this.debtFunds = 0;
            ToppedUp = null;
            Withdrawn = null;
        }

        public bool TopUp(decimal amount)
        {
            if(amount > 0m)
            {
                if (debtFunds < 0m)
                {
                    decimal result = debtFunds + amount;
                    if (result >= 0m)
                    {
                        ownFunds += result;
                        debtFunds = 0;
                    }
                    else
                    {
                        debtFunds = result;  
                    }
                    var args = new BalanceEventArg($"The balance was topped up on {amount} {Settings.СurrencyName}.", amount);
                    ToppedUp?.Invoke(this, args);
                    return true;
                }
                else
                {
                    var args = new BalanceEventArg($"The balance was topped up on {amount} {Settings.СurrencyName}.", amount);
                    ToppedUp?.Invoke(this, args);
                    return true;
                }
            }
            else
            {
                var args = new BalanceEventArg($"Declined due to the forbidden argument {amount}.", amount);
                ToppedUp?.Invoke(this, args);
                return false;
            }           
        }

        public bool Withdraw(decimal amount)
        {
            if(amount > 0m)
            {
                if (ownFunds >= amount)
                {
                    ownFunds -= amount;
                    var args = new BalanceEventArg($"The {amount} {Settings.СurrencyName} was withdrawned from the balance.", amount);
                    Withdrawn?.Invoke(this, args);
                    return true;
                }
                else
                {
                    var args = new BalanceEventArg($"Not enough money on balance.", amount);
                    Withdrawn?.Invoke(this, args);
                    return false;
                }
            }
            else
            {
                var args = new BalanceEventArg($"Incorrect operation. Declined due to the forbidden argument {amount}.", amount);
                Withdrawn?.Invoke(this, args);
                return false;
            }
        }

        public bool WithdrawWithDebt(decimal amount)
        {
            if (amount > 0m)
            {
                decimal result = ownFunds - amount;
                if (result < 0m)
                {
                    debtFunds += result;
                    ownFunds = 0;
                    var args = new BalanceEventArg($"The {amount} {Settings.СurrencyName} was withdrawned for the balance with {result} {Settings.СurrencyName} debt.", amount);
                    Withdrawn?.Invoke(this, args);
                    return true;
                }
                else
                {
                    ownFunds -= amount;
                    var args = new BalanceEventArg($"The {amount} {Settings.СurrencyName} was withdrawned from the balance.", amount);
                    Withdrawn?.Invoke(this, args);
                    return true;
                }
            }
            else
            {
                var args = new BalanceEventArg($"Incorrect operation. Declined due to the forbidden argument {amount}.", amount);
                Withdrawn?.Invoke(this, args);
                return false;
            }
        }

        public decimal GetOwnFunds()
        {
            return ownFunds;
        }

        public decimal GetDebtFunds()
        {
            return debtFunds;
        }

        public decimal GetBalance()
        {
            return debtFunds + ownFunds;
        }
    }
}