﻿using System;

namespace Parking
{
    class Transaction
    {
        public DateTime Time { get; }
        public string Description { get; }
        public Guid Guid { get; }
        public override string ToString() => this.Time + " -> " + this.Guid + ": " + this.Description;

        public Transaction(Guid guid, string description)
        {
            this.Time = DateTime.Now;
            this.Guid = guid;
            this.Description = description;
        }
    }
}
