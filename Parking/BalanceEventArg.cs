﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Parking
{
    public class BalanceEventArg
    {
        public string ActionMessage { get; }
        public decimal Amount { get; }

        public BalanceEventArg(string actionMessage, decimal amount)
        {
            ActionMessage = actionMessage;
            Amount = amount;
            Console.WriteLine(ToString());
            var transactionLogger = TransactionLogger.GetInstance();
            transactionLogger.WriteLog(ToString());
        }

        public override string ToString() => this.ActionMessage;
    }
}
