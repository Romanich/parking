﻿using System;
using System.Linq;

namespace Parking
{
    class Parking
    {
        private static readonly Parking parking = new Parking();
        public ParkingSlot[] ParkingSlot { get; private set; }
        public Balance ParkingBalance { get; }

        private Parking()
        {
            ParkingSlot = new ParkingSlot[Settings.MaximumParkingCapacity];
            ParkingBalance = new Balance(0m);
            RunInterval();
        }

        public static Parking GetInstance()
        {
            return parking;
        }

        public void ShowParkingBalance()
        {
            Console.WriteLine($"Current parking's balance: {ParkingBalance.GetBalance()} {Settings.СurrencyName}");
        }

        public void ShowFreeAndUsedSlots()
        {
            var free = ParkingSlot.Where(slot => slot == null).Count();
            Console.WriteLine($"Total parking slots: {ParkingSlot.Length}");
            Console.WriteLine($"Free parking slots: {free}");
            Console.WriteLine($"Used parking slots: {ParkingSlot.Length - free}");
        }

        public void ShowAllSlotsDetails()
        {
            for (int i = 0; i < ParkingSlot.Length; i++)
            {
                Console.WriteLine();
                Console.Write($"Slot {i}: ");
                Console.Write(ParkingSlot[i] == null ? " is empty. \n" : ParkingSlot[i].GetSlotInfoDetails());
            }
        }

        public void ShowUsedSlotsShortInfo()
        {
            Console.WriteLine("List of parked vehicals:");
            for (int i = 0; i < ParkingSlot.Length; i++)
            {
                if (ParkingSlot[i] != null) Console.WriteLine($"Slot {i}: {ParkingSlot[i].GetVehiclelId()}");
            }
        }

        public void ParkVehicle()
        {
            Console.WriteLine("4. Park a vehicle \n");
            if (ParkingSlot.Where(slot => slot == null).Any() == false)
            {
                Console.WriteLine("All parking slots are used already!");
                return;
            }
            Console.WriteLine("Select vehicle type:");
            Console.WriteLine("1. Car");
            Console.WriteLine("2. Motorcycle");
            Console.WriteLine("3. Bus");
            Console.WriteLine("4. Truck");
            var input = Console.ReadLine();

            Vehicle vehicle;
            switch (input)
            {
                case "1": vehicle = new Vehicle(Settings.VehicleTypes.Car); break;
                case "2": vehicle = new Vehicle(Settings.VehicleTypes.Motorcycle); break;
                case "3": vehicle = new Vehicle(Settings.VehicleTypes.Bus); break;
                case "4": vehicle = new Vehicle(Settings.VehicleTypes.Truck); break;
                default: Console.WriteLine("Incorrect input, vehical was not parked..."); return;
            }

            for (int i = 0; i < ParkingSlot.Length; i++)
            {
                if (ParkingSlot[i] == null)
                {
                    ParkingSlot[i] = new ParkingSlot();
                    ParkingSlot[i].SetVehicle(vehicle);
                    break;
                }
            }
        }

        public void RemoveVehicle()
        {
            Console.WriteLine("6. Remove a vehicle");
            ShowUsedSlotsShortInfo();
            Console.Write("Input parking slot number to remove vehicle: ");

            uint slotNumber = GetSlotNumberInput();

            if (ParkingSlot[slotNumber] != null)
            {
                Console.WriteLine($"Vehicle {ParkingSlot[slotNumber].GetVehiclelId()} will be removed from parking!");
                bool isConfirmed = IsConfirmed();
                if (isConfirmed && ParkingSlot[slotNumber].SlotVehicle.Balance.GetBalance() >= 0m)
                {
                    Console.WriteLine($"Vehical {ParkingSlot[slotNumber].GetVehiclelId()} was removed from parking.");
                    ParkingSlot[slotNumber] = null;
                }
                else if (!isConfirmed)
                {
                    Console.WriteLine("Operation was canceled.");
                }
                else
                {
                     Console.WriteLine($"Vehical {ParkingSlot[slotNumber].GetVehiclelId()} is blocked due to debts. " +
                                       $"Top up balance in {decimal.Negate(ParkingSlot[slotNumber].SlotVehicle.Balance.GetDebtFunds())} {Settings.СurrencyName} " +
                                       $"to remove {ParkingSlot[slotNumber].GetVehiclelId()}.");
                }
            }
            else Console.WriteLine("Slot is empty. Check your input.");
        }

        public void TopUpVehicleBalance()
        {
            Console.WriteLine("7. Top up balance of parked vehical");
            ShowUsedSlotsShortInfo();
            Console.WriteLine("Input ID of the parked vehical to top up it balance: ");

            var input = Console.ReadLine();
            var balance = ParkingSlot.Where(x => x?.SlotVehicle?.Id == input).Select(x => x?.SlotVehicle.Balance).FirstOrDefault();
            if (balance == null)
            {
                Console.WriteLine("Billing for vehical with this ID wasn't found!");
            }
            else
            {
                Console.WriteLine($"Input amount of {Settings.СurrencyName} for increasement: ");
                var moneyInput = GetMoneyInput();
                if (IsConfirmed())
                {
                    balance.TopUp(moneyInput);
                    Console.WriteLine($"Balance now is {balance.GetBalance()} {Settings.СurrencyName}");
                }
            }
        }

        private uint GetSlotNumberInput()
        {
            var input = Console.ReadLine();
            uint slotNumberInput;
            while (!uint.TryParse(input, out slotNumberInput) || (slotNumberInput >= Settings.MaximumParkingCapacity))
            {
                Console.WriteLine("Incorect iput. Make your retry... ");
                input = Console.ReadLine();
            }
            return slotNumberInput;
        }

        private bool IsConfirmed()
        {
            Console.WriteLine("Type 'y' to confirm");
            if (Console.ReadLine().ToLower() == "y")
            {
                return true;
            }  
            else
            {
                Console.WriteLine("Was declined.");
                return false;
            }
        }

        private decimal GetMoneyInput()
        {
            var input = Console.ReadLine();
            decimal decimalInput;
            while (!decimal.TryParse(input, out decimalInput) || decimalInput <= 0m)
            {
                input = Console.ReadLine();
            }
            return decimalInput;
        }

        private void GetCharge()
        {
            foreach (ParkingSlot slot in ParkingSlot)
            {
                decimal charge = slot?.GetSlotCharge() ?? 0m;
                if (charge > 0m)
                {
                    ParkingBalance.TopUp(charge);
                } 
            }
        }

        private void RunInterval()
        {
            var timer = new System.Threading.Timer((e)
                => { GetCharge(); }, null, TimeSpan.Zero, TimeSpan.FromSeconds(Settings.WithdrawalInterval));
        }
    }
}
