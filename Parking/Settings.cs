﻿using System;

namespace Parking
{
    class Settings
    {
        public static uint MaximumParkingCapacity = 10;
        public static double WithdrawalInterval = 5;
        public static decimal OverusedCostTaxRate = 2.5m;
        public static decimal DefaultCostStartAmount = 10m;
        public static string СurrencyName = "UAH";

        public enum VehicleTypes
        {
            Bus,
            Motorcycle,
            Truck,
            Car
        }

        public static decimal GetIntervalChargeCost(VehicleTypes vehicleTypes)
        {
            switch (vehicleTypes) {
                case VehicleTypes.Bus: return 3.2m;
                case VehicleTypes.Motorcycle: return 1m;
                case VehicleTypes.Truck: return 5m;
                case VehicleTypes.Car: return 2m;
                default: return 1m;
            }
        }
    }
}
