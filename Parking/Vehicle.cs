﻿using System;
using static Parking.Settings;

namespace Parking
{
    class Vehicle
    {
        public String Id { get; private set; }
        public VehicleTypes VehicleType { get; private set; }
        public Balance Balance { get; }

        public Vehicle(VehicleTypes vehicleType)
        {
            Id = GetRandomId();
            VehicleType = vehicleType;
            Balance = new Balance(10m);
        }

        private string GetRandomId()
        {
            string id = Guid.NewGuid().ToString().Replace("-", string.Empty)
                .Replace("+", string.Empty).Substring(0, 4);
            return id;
        }
    }
}
