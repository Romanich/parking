﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Parking
{
    class TransactionLogger
    {
        string path = $"..\\Transaction_{DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss")}.log";

        private static Dictionary<DateTime, string> Transactions = new Dictionary<DateTime, string>();

        private static readonly TransactionLogger transactionLogger = new TransactionLogger();

        private TransactionLogger() { }

        public static TransactionLogger GetInstance() 
        {
            return transactionLogger;
        }

        public void WriteLog(string transaction)
        {
            try
            {
                using (FileStream fstream = new FileStream(path, FileMode.OpenOrCreate))
                {
                    byte[] logText = Encoding.Default.GetBytes(transaction + "\n");
                    fstream.Seek(0, SeekOrigin.End);
                    fstream.Write(logText);
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                Console.WriteLine("\nFile is not accesible! Check permissions for current user... " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("\nSomething went wrong... " + ex.Message);
            }
        }

        public void ReadLog()
        {
            try
            {
                using (FileStream fstream = new FileStream(path, FileMode.OpenOrCreate))
                {
                    fstream.Seek(0, SeekOrigin.Begin);
                    var output = new byte[fstream.Length];
                    fstream.Read(output, 0, output.Length);
                    Console.WriteLine("Transation information: \n" + Encoding.Default.GetString(output));
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                Console.WriteLine("\nUnauthorized access! Check permissions for current user." + ex.Message);
            }
            catch(FileNotFoundException ex)
            {
                Console.WriteLine("\nSelected file was not found... " + ex.Message);
            }
            catch (IOException ex)
            {
                Console.WriteLine("\nUnable to load file... " + ex.Message);
            }

            catch (Exception ex)
            {
                Console.WriteLine("\nSomething went wrong... " + ex.Message);
            }
        }

        public void ReadTransactionsForPeriod(DateTime fromTime)
        {
            Console.WriteLine("Transaction list for period of time: \n");
            var outputList = Transactions.Where(x => x.Key > fromTime).Select(x => x.Value).ToList();

            foreach (var item in outputList)
                Console.WriteLine(item + "\n");
        }
    }
}
